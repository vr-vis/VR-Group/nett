#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file

#include <zmq.h>
#include <cassert>
#include <string>
#include <iostream>

#include <catch.hpp> //note: should always go after "include <zmq.h>" because it includes windows.h which clashes with zmq header!

#include <nett/slot_address.h>

using namespace nett;

bool is_equal( const slot_address &s1, const slot_address &s2 )
{
    return s1 == s2;
}

bool address_string_is_equal( const slot_address &s1, const slot_address &s2 )
{
    return s1.address_to_string() == s2.address_to_string();
}

TEST_CASE( "slot_address", "[nett_slots]" )
{
    slot_address slot_address_1( "tcp://127.0.0.1:6555", "time_event" );
    slot_address slot_address_2( "tcp://127.0.0.1:6555", "time_event" );
    slot_address slot_address_3( "tcp://127.0.0.1:6555", "spike_event" );
    slot_address slot_address_4( "tcp://127.0.0.1:222", "spike_event" );

    REQUIRE( is_equal (slot_address_1, slot_address_2 ) == true );
    REQUIRE( is_equal (slot_address_1, slot_address_3 ) == false );
    REQUIRE( is_equal (slot_address_3, slot_address_4 ) == false );

    REQUIRE( address_string_is_equal (slot_address_1, slot_address_2 ) == true );
    REQUIRE( address_string_is_equal (slot_address_1, slot_address_2 ) == true );
    REQUIRE( address_string_is_equal (slot_address_2, slot_address_3 ) == true );
    REQUIRE( address_string_is_equal (slot_address_3, slot_address_4 ) == false );





}
