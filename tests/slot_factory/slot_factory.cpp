#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file

#include <zmq.h>
#include <cassert>
#include <string>
#include <iostream>
#include <functional>

#include <catch.hpp> //note: should always go after "include <zmq.h>" because it includes windows.h which clashes with zmq header!

#include <nett/detail/context.h>
#include <nett/detail/socket_publisher.h>
#include <nett/detail/socket_subscriber.h>
#include <nett/utils.h>

#include <nett/detail/slot_base.h>
#include <nett/detail/publisher_threadsafe.h>
#include <nett/slot_out.h>

#include <nett/schema/string_message.pb.h>
#include <nett/schema/routing_message.pb.h>

#include <memory>
#include <thread>

bool publish_message_with_header(const nett::socket_publisher &publisher, const nett::byte_stream &content, const nett::byte_stream &header)
{   
    std::this_thread::sleep_for(std::chrono::milliseconds(10));

    for(int i = 0; i < 10; i++)
        publisher.send( content, header );

    return true;
}

bool receive_message_with_header( const nett::socket_subscriber &subscriber, const nett::byte_stream &routing_condition)
{
    nett::message_t message = subscriber.receive();

    routing_message routing_condition_message = nett::deserialize_unchecked<routing_message>(routing_condition);
    routing_message routing_received = nett::deserialize_unchecked<routing_message>(message.routing);

    if (routing_received.socket() != routing_condition_message.socket() )
        return false;
    if (routing_received.slot_tag() != routing_condition_message.slot_tag() )
        return false;

    //all have passed
    return true;
}

void on_string_event( const string_message &msg )
{
    std::cout << "got called" << std::endl;
}

void on_string_event_unpacked( const string_message &msg )
{
    msg.SerializeToOstream( &std::cout );
}


class slot_in_base : public nett::slot_base
{
public:
    virtual void receive(const nett::byte_stream &content) = 0;

protected:
    slot_in_base( const std::string &slot_tag )
        : slot_base( slot_tag )
    {
    }

    virtual ~slot_in_base()
    {
    }

};

template <class T>
class slot_in final : public slot_in_base
{
public:
    using event_type = T;
    using on_event_function = std::function< void(const event_type&) >;

    slot_in( const std::string &slot_tag )
        : slot_in_base( slot_tag )
    {}

    virtual void receive( const nett::byte_stream &content ) override
    {
        event_type event = nett::deserialize_checked< event_type >( content );

        for ( auto &f : function_container_ )
            f( event );
    }

    void on_event( on_event_function function )
    {
        function_container_.push_back( function );
    }

private:

    std::vector< on_event_function > function_container_;
};

class content_deliverer final
{
public:

    void register_slot( std::shared_ptr<slot_in_base> slot_ptr )
    {
        //lock insert, we could try to receive concurrently
        std::lock_guard<std::mutex> lock_guard( mutex_ );
        slot_in_map_[ slot_ptr->get_slot_tag() ] = slot_ptr; //todo: check for existing slots
    }

    void deliver_message( const std::string &slot_tag, nett::byte_stream &content)
    {
        //and deliver to slot, block while we iterate the map
        std::lock_guard<std::mutex> lock_guard( mutex_ );
        auto it = slot_in_map_.find( slot_tag );

        if ( it != slot_in_map_.end() )
            it->second->receive( content );
    }

private:
    std::map<std::string, std::shared_ptr< slot_in_base >> slot_in_map_;
    std::mutex mutex_;
};

class slot_factory final
{
public:
    slot_factory( const std::string &publisher_endpoint, const std::string &subscriber_endpoint )
        : publisher_threadsafe_( context_, publisher_endpoint )
        , subscriber_( context_, subscriber_endpoint )
        , thread_( &slot_factory::deliver, this )
    {
    }
    
    ~slot_factory()
    {
        running_ = false; //signal thread to stop
        thread_.join();
    }
    
    template <class event_type>
    std::shared_ptr< nett::slot_out <event_type > > make_slot_out(const std::string &slot_tag)
    {
        return std::make_shared<nett::slot_out <event_type> >( slot_tag, publisher_threadsafe_ );
    }
    
    template <class event_type>
    std::shared_ptr< slot_in <event_type> > make_slot_in( const std::string &slot_tag )
    {
        std::shared_ptr< slot_in <event_type> > slot_ptr = std::make_shared< slot_in < event_type > >( slot_tag );
        content_deliverer_.register_slot( slot_ptr );
        return slot_ptr;
    }

private:

    void deliver()
    {
        while ( running_ )
        {
            nett::message_t message = subscriber_.receive();

            //look into routing message to deliver to right in slot
            routing_message routing = nett::deserialize_unchecked<routing_message>( message.routing );
            content_deliverer_.deliver_message( routing.slot_tag(), message.content );
        }
    }

    nett::context context_;
    const nett::socket_subscriber subscriber_;
    nett::publisher_threadsafe publisher_threadsafe_;  
    content_deliverer content_deliverer_;
    
    std::thread thread_;
    bool running_ = true; 
};

TEST_CASE( "create_slots", "[nett_slots]" )
{
    const std::string endpoint( "tcp://127.0.0.1:6555" );

    GOOGLE_PROTOBUF_VERIFY_VERSION;

    slot_factory slot_fac( endpoint, endpoint );

    auto slotOut = slot_fac.make_slot_out<string_message>("string_event");
    auto slotIn = slot_fac.make_slot_in<string_message>("string_event");
    slotIn->on_event(&on_string_event);
    slotIn->on_event(&on_string_event_unpacked);

    string_message string_message_one;
    string_message_one.set_value("publish this via slot");

    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    for (int i = 0 ; i < 10; ++i)
    slotOut->send( string_message_one );

    while( true )
        int a = 2;

    google::protobuf::ShutdownProtobufLibrary();
}
