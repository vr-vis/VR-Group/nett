#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file

#include <zmq.h>
#include <cassert>
#include <string>
#include <iostream>

#include <catch.hpp> //note: should always go after "include <zmq.h>" because it includes windows.h which clashes with zmq header!

#include <nett/detail/context.h>
#include <nett/detail/socket_publisher.h>
#include <nett/detail/socket_subscriber.h>
#include <nett/utils.h>

#include <nett/schema/string_message.pb.h>
#include <nett/schema/routing_message.pb.h>



bool publish_message_with_header(const nett::socket_publisher &publisher, const nett::byte_stream &content, const nett::byte_stream &header)
{   
    for(int i = 0; i < 10; i++)
        publisher.send( content, header );

    return true;
}

bool receive_message_with_header( const nett::socket_subscriber &subscriber, const nett::byte_stream &routing_condition)
{
    nett::message_t message= subscriber.receive();

    routing_message routing_condition_message = nett::deserialize_unchecked<routing_message>(routing_condition);
    routing_message routing_received = nett::deserialize_unchecked<routing_message>(message.routing);

    if (routing_received.socket() != routing_condition_message.socket() )
        return false;
    if (routing_received.slot_tag() != routing_condition_message.slot_tag() )
        return false;

    //all have passed
    return true;
}

TEST_CASE( "publish_subscribe_to_topic", "[nett_basics]" )
{
    const std::string endpoint( "tcp://127.0.0.1:6555" );

    GOOGLE_PROTOBUF_VERIFY_VERSION;

    //create routing header
    routing_message routing_message_sub1;
    routing_message_sub1.set_socket( endpoint );
    routing_message_sub1.set_slot_tag( "sub1" );

    routing_message routing_message_sub2;
    routing_message_sub2.set_socket( endpoint );
    routing_message_sub2.set_slot_tag( "sub2" );

    nett::context context;
    nett::socket_publisher publisher( context, endpoint );
    nett::socket_subscriber subscriber_sub1( context, endpoint, nett::serialize_unchecked( routing_message_sub1 ) );
    nett::socket_subscriber subscriber_sub2( context, endpoint, nett::serialize_unchecked( routing_message_sub2 ) ); 
    
    string_message message;
    message.set_value( "get this published!" );

    REQUIRE( publish_message_with_header( publisher, nett::serialize_checked( message ), nett::serialize_unchecked( routing_message_sub1 ) ) == true );
    REQUIRE( publish_message_with_header( publisher, nett::serialize_checked( message ), nett::serialize_unchecked( routing_message_sub2 ) ) == true );
    
    REQUIRE( receive_message_with_header( subscriber_sub1, nett::serialize_unchecked( routing_message_sub1) ) == true );
    REQUIRE( receive_message_with_header( subscriber_sub2, nett::serialize_unchecked( routing_message_sub2) ) == true );

    google::protobuf::ShutdownProtobufLibrary();
}
