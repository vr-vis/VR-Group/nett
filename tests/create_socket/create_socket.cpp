#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file

#include <zmq.h>
#include <cassert>
#include <string>

#include <catch.hpp> //note: should always go after including zmq.h, because it includes windows.h which clashes with zmq header!

int create_socket()
{
    const std::string endpoint("tcp://*:5555");

    //-------------context creation----------------------//
    void *context_ptr = zmq_ctx_new();
    assert(context_ptr != nullptr);

    if ( context_ptr == nullptr )
        return 1; //indicate error
    
    //-------------socket creation----------------------//
    void *socket_ptr = zmq_socket( context_ptr, ZMQ_REP );
    assert( socket_ptr != nullptr );

    if ( socket_ptr == nullptr )
    {
        zmq_ctx_term(context_ptr); //cleanup
        return 1; //indicate error
    } 
    
    //-------------socket binding----------------------//
    int zmq_bind_return_value = zmq_bind( socket_ptr, endpoint.c_str() );

    if ( zmq_bind_return_value != 0)
    {
        zmq_close( socket_ptr );
        zmq_ctx_term( context_ptr );
        return zmq_bind_return_value;
    }
    
    //-------------socket rollback----------------------//
    int zmq_unbind_return_value = zmq_unbind( socket_ptr, endpoint.c_str() );

    if ( zmq_unbind_return_value != 0 )
    {
        zmq_close( socket_ptr );
        zmq_ctx_term( context_ptr );
        return zmq_unbind_return_value;
    }

    int zmq_close_return_value = zmq_close( socket_ptr );
    
    if ( zmq_close_return_value == 0 )
    {
        socket_ptr = nullptr;
    }
    else
    {
        zmq_ctx_term( context_ptr );
        return zmq_close_return_value;
    }

    int zmq_ctx_term_return_value = zmq_ctx_term( context_ptr );

    if ( zmq_ctx_term_return_value == 0 )
    {
        context_ptr = nullptr;
    }
        
    return zmq_bind_return_value;
}

TEST_CASE( "ZMQ socket is created", "[zmq_basics]" )
{
    REQUIRE( create_socket() == 0 );   
}
