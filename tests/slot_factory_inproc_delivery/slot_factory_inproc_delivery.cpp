#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file

#include <cassert>
#include <string>
#include <iostream>
#include <thread>
#include <chrono>
#include <memory>


#include <nett/nett.h>
#include <nett/schema/string_message.pb.h>
//#include <../schema/int_message.pb.h>

#include <catch.hpp> //note: should always go after "include <zmq.h>" because it includes windows.h which clashes with zmq header!


void on_string_event_unpacked( const string_message &msg )
{
    msg.SerializeToOstream( &std::cout );
}

//void on_int_event_unpacked(const int_message &msg)
//{
//	msg.SerializeToOstream(&std::cout);
//}

TEST_CASE( "create_slots", "[nett_slots]" )
{
    const std::string endpoint( "tcp://127.0.0.1:6555" );
    const std::string endpoint2( "tcp://127.0.0.1:6556" );

    nett::initialize( endpoint );

    auto slotOut = nett::make_slot_out<string_message>( "string_event" );
    //auto slotOut_2 = nett::make_slot_out<string_message>( "time_event" );
	//auto slotOut_4 = nett::make_slot_out<int_message>("int_event");
    
    auto slotIn = nett::make_slot_in<string_message>();
    auto slotIn_2 = nett::make_slot_in<string_message>();
    auto slotIn_3 = nett::make_slot_in<string_message>();
    auto slotIn_4 = nett::make_slot_in<string_message>();

    auto slotIn_5 = nett::make_slot_in<string_message>();
    auto slotIn_6 = nett::make_slot_in<string_message>();
    auto slotIn_7 = nett::make_slot_in<string_message>();
    
    /*nett::slot_factory slot_fac2( endpoint2 );
    auto slotOut_3 = slot_fac2.make_slot_out<string_message>( "time_event" );*/
    
    
    slotIn->connect( nett::slot_address( endpoint, "string_event" ) );
    slotIn_2->connect( nett::slot_address( endpoint, "string_event" ) );
    slotIn_3->connect( nett::slot_address( endpoint, "string_event" ) );
    slotIn_4->connect( nett::slot_address( endpoint, "string_event" ) );

    slotIn_5->connect( nett::slot_address( endpoint, "string_event" ) );
    slotIn_6->connect( nett::slot_address( endpoint, "string_event" ) );
    slotIn_7->connect( nett::slot_address( endpoint, "string_event" ) );
	//slotIn_3->connect(nett::slot_address(endpoint, "int_event"));

    std::this_thread::sleep_for( std::chrono::seconds( 1 ) ); //give time to communicate subscription topics

    string_message string_message_one;
    string_message_one.set_value( "publish as string_event" );

    for (int i = 0 ; i < 1; ++i)
        slotOut->send( string_message_one );
    
    for (int i = 0; i < 1; ++i )
    {
        on_string_event_unpacked( slotIn->receive() );
        on_string_event_unpacked( slotIn_2->receive() );
        on_string_event_unpacked( slotIn_3->receive() );
        on_string_event_unpacked( slotIn_4->receive() );
    }
}
