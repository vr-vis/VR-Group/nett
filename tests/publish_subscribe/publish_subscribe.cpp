#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file

#include <zmq.h>
#include <cassert>
#include <string>
#include <iostream>
#include <chrono>
#include <thread>

#include <catch.hpp> //note: should always go after "include <zmq.h>" because it includes windows.h which clashes with zmq header!

#include <nett/detail/context.h>
#include <nett/detail/socket_publisher.h>
#include <nett/detail/socket_subscriber.h>



#include <nett/schema/string_message.pb.h>


bool publish_message(const nett::socket_publisher &publisher, const string_message &message)
{   
    std::this_thread::sleep_for(std::chrono::milliseconds(100)); //give subscriber time to connect
    for(int i = 0; i < 100; i++)
        publisher.send( nett::serialize_checked<string_message>(message) );

    return true; //okay, this test cannot really fail ;)
}

bool receive_message(const nett::socket_subscriber &subscriber, const string_message &msg )
{
    nett::message_t message = subscriber.receive();
    string_message result = nett::deserialize_unchecked<string_message>(message.content);

    return ( result.value() == msg.value() );
}

TEST_CASE( "publish_subscribe", "[nett_basics]" )
{
    const std::string endpoint("tcp://127.0.0.1:6555");

    GOOGLE_PROTOBUF_VERIFY_VERSION;

    nett::context context;
    nett::socket_publisher publisher( context, endpoint );
    nett::socket_subscriber subscriber( context, endpoint );

    string_message message;
    message.set_value( "get this published!" );

    REQUIRE( publish_message( publisher, message ) == true );
    REQUIRE( receive_message( subscriber, message ) == true );

    google::protobuf::ShutdownProtobufLibrary();
}
