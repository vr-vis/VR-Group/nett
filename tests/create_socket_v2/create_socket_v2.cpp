#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file

#include <zmq.h>
#include <cassert>
#include <string>

#include <catch.hpp> //note: should always go after including zmq.h, because it includes windows.h which clashes with zmq header!

#include <nett/detail/context.h>
#include <nett/detail/socket_base.h>

namespace nett
{
    class socket_reply : public socket_base //note: each socket type should derive from socket_base, e.g., subscriber, publisher, dealer and router
    {
    public:
        socket_reply(const context &context)
            : socket_base(context)
        {
            socket_ = zmq_socket(context_.get(), ZMQ_REP);
            assert(socket_ != nullptr);

            if ( socket_ == nullptr )
                throw std::runtime_error("Construction of zmq reply socket failed");
        }
    };
 
} //namespace nett


int create_socket()
{
    const std::string endpoint("tcp://*:5555");

    //-------------context creation----------------------//
    nett::context context;
    //-------------socket creation----------------------//
    nett::socket_reply socket(context);
    //-------------socket binding----------------------//
    //if ( socket.bind( endpoint ) == false )
    //    return 1;

    //if ( socket.unbind( endpoint ) == false )
    //    return 1;

    return 0;
}

TEST_CASE( "ZMQ socket is created", "[zmq_basics]" )
{
    REQUIRE( create_socket() == 0 );   
}
