cmake_minimum_required(VERSION 3.0)

project ( nett )
set( nett_VERSION 0.0.1 )

# Expand available module directories by appending ./CMake directory.
list( APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/CMake )

#include macro for event compilation
include( CMakeHelpers )

include( CTest )

include( "include/nett/_SourceFiles.cmake" )
include( "src/nett/_SourceFiles.cmake" )

option(nett_BUILD_SHARED_LIBS "Enable dynamic linkage." OFF)
if (nett_BUILD_SHARED_LIBS)
  set(nett_SHARED_OR_STATIC "SHARED")
else (nett_BUILD_SHARED_LIBS)
  set(nett_SHARED_OR_STATIC "STATIC")
endif ()

if( MSVC )

	set( LIBZMQ_INCLUDE_DIR "" CACHE PATH "Path to the include directory of zmq" )
	set( LIBZMQ_LIBRARY "" CACHE FILEPATH "The zmq library to link against" )

	set( PROTOBUF_INCLUDE_DIR "" CACHE PATH "The protobuf include directory" )
	set( PROTOBUF_LIBRARY "" CACHE FILEPATH "The protobuf library to link against" )
	set( PROTOBUF_LIBRARY_DEBUG "" CACHE FILEPATH "The protobuf library to link against" )
	set( PROTOBUF_PROTOC_EXECUTABLE "" CACHE FILEPATH "The protobuf binary directory with the schema compiler 'protoc'" )

	set ( LINK_AGAINST_DLL ON CACHE BOOL "Set this to true if you intend to link this lib against a DLL" )
	if ( LINK_AGAINST_DLL )
	  add_definitions("-DBUILD_DLL")
	endif()

elseif( UNIX )

	# Include custom Findlibzmq file to allow automatic finding.
	include( Findlibzmq )

	set( CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -std=c++14 -fPIC" )

	# Find required libraries.
	find_package( Protobuf REQUIRED )
	find_package( libzmq REQUIRED )
	find_package( Threads REQUIRED )

	set( UNIX_PTHREADS ${CMAKE_THREAD_LIBS_INIT} )

endif(  )

include_directories (${PROJECT_SOURCE_DIR})
include_directories (${PROTOBUF_INCLUDE_DIR})
include_directories (${LIBZMQ_INCLUDE_DIR})

include_directories ("${CMAKE_CURRENT_SOURCE_DIR}/include")
include_directories ("${CMAKE_CURRENT_SOURCE_DIR}/schema")
include_directories ("${PROJECT_BINARY_DIR}")

add_event ( nett routing_message schema)

if(NOT CMAKE_DEBUG_POSTFIX)
  set(CMAKE_DEBUG_POSTFIX d)
endif()

install(FILES
  "${PROJECT_BINARY_DIR}/nett/schema/routing_message.pb.h"
  DESTINATION include/nett/schema
  COMPONENT dev )
add_library ( nett ${nett_SHARED_OR_STATIC} ${ProjectSources} ${nett_EVENT_SOURCE_FILES} )
target_link_libraries( nett ${LIBZMQ_LIBRARY} ${UNIX_PTHREADS} )
target_link_libraries( nett optimized ${PROTOBUF_LIBRARY} debug ${PROTOBUF_LIBRARY_DEBUG})

set( CMAKE_MODULE_INSTALL_PATH "share/nett/CMake")
configure_file(
  CMake/CMakeHelpers.cmake
  ${PROJECT_BINARY_DIR}/${CMAKE_MODULE_INSTALL_PATH}/CMakeHelpers.cmake
  COPYONLY
  )
install( FILES CMake/CMakeHelpers.cmake
  DESTINATION ${CMAKE_MODULE_INSTALL_PATH} COMPONENT dev )

include(GenerateConfig)

add_subdirectory ( tests )
add_subdirectory ( examples )


include(CPackConfig)
