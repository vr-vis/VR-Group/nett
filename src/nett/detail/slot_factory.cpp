#include <zmq.h>

#include <nett/detail/slot_factory.h>
#include <nett/schema/routing_message.pb.h>

#include <thread>
#include <chrono>
#include <algorithm>
#include <iostream>

nett::slot_factory::slot_factory( const std::string publisher_endpoint )
    : publisher_threadsafe_( context_, publisher_endpoint )
    , run_thread_(true)
    , subscriber_( socket_subscriber(context_))
    , thread_( &slot_factory::deliver, this )
    , inproc_publisher_( context_, inproc_endpoint_ )
    , connect_mutex_(new std::mutex() )
{
    GOOGLE_PROTOBUF_VERIFY_VERSION;
}

nett::slot_factory::~slot_factory()
{
    //send a message to terminate blocking receive call in deliver
    kill();
    thread_.join();

    google::protobuf::ShutdownProtobufLibrary();
#ifndef BUILD_DLL
	delete connect_mutex_;
#endif
}

bool nett::slot_factory::connect_in_slot( const std::string endpoint )
{
    //note: must be thread safe
    if ( !is_connected( endpoint ) ) //check if connection is already established, if so drop
    {
        //note: must be thread safe      
        std::lock_guard< std::mutex > guard( *connect_mutex_ );
        bool return_value = subscriber_.connect( endpoint );
        return return_value;
    }
    else //already connected to there
    {
        std::cout << "connection to this endpoint already exits, dropping connection attempt!" << std::endl;
        return true;
    }
}

void nett::slot_factory::disconnect_in_slot( const std::string endpoint )
{
    //note: must be thread safe
    remove_connection( endpoint );
    {
        //note: must be thread safe      
        std::lock_guard< std::mutex > guard( *connect_mutex_ );
        subscriber_.disconnect( endpoint );
    }
    std::cout << "successfully disconnected slot by un-subscribing" << std::endl;
}

void nett::slot_factory::deliver()
{
    while ( run_thread_ )
    {
        connect_mutex_->lock();
        nett::message_t message;
        zmq_pollitem_t item;

        item.socket = subscriber_.get();
        item.events = ZMQ_POLLIN;

        int zmq_poll_count = zmq_poll( &item, 1, poll_time_ );

        if ( zmq_poll_count > 0 )
        {
            message = subscriber_.receive();

            routing_message routing = nett::deserialize_unchecked< routing_message >( message.routing );

            //just pass the message 'as is' to the inproc publisher
            inproc_publisher_.send( message.content, message.routing );
            connect_mutex_->unlock();
        }
        else
        {
            connect_mutex_->unlock();
            std::this_thread::sleep_for( std::chrono::microseconds( poll_time_ ) );//note: give time to other threads trying to lock mutex
        }
    }
}

void nett::slot_factory::kill()
{
	run_thread_ = false;
}

bool nett::slot_factory::is_connected(const std::string endpoint)
{
    auto it = std::find(endpoint_connections_.begin(), endpoint_connections_.end(), endpoint);

    if ( it == endpoint_connections_.end() ) //not in vector, so add
    {
        endpoint_connections_.push_back( endpoint );
        return false;
    }
    else
        return true;
}

void nett::slot_factory::remove_connection( const std::string endpoint )
{
    auto it = std::find(endpoint_connections_.begin(), endpoint_connections_.end(), endpoint);

    if ( it == endpoint_connections_.end() )
    {
        std::cout <<  "slot to disconnect does not seem to be registered properly" << std::endl;
        return;
    }

    endpoint_connections_.erase( it );
}