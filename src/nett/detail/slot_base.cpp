#include <nett/detail/slot_base.h>

const std::string nett::slot_base::get_slot_tag() const
{
    return slot_tag_;
}

nett::slot_base::slot_base( const std::string &slot_tag ) : slot_tag_( slot_tag )
{

}

nett::slot_base::~slot_base()
{
}