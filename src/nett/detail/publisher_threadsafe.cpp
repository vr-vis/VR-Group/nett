#include <nett/detail/publisher_threadsafe.h>

#include <nett/schema/routing_message.pb.h>

namespace nett {

publisher_threadsafe::publisher_threadsafe( const nett::context &context, const std::string endpoint )
    : publisher_( context, endpoint )
    , endpoint_( endpoint )
{
}

void publisher_threadsafe::send( const std::string slot_tag, const nett::byte_stream &content )
{
    mutex_.lock();
    routing_message routing;
    routing.set_socket( endpoint_ );
    routing.set_slot_tag( slot_tag );

    //lock publisher while sending
    publisher_.send( content, nett::serialize_unchecked( routing ) );
    mutex_.unlock();
}

} //namespace nett
