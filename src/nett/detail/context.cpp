#include <nett/detail/context.h>

#include <zmq.h>
#include <cassert>
#include <stdexcept>

namespace nett
{

context::context()
{
    context_ = zmq_ctx_new();
    assert( context_ != nullptr );

    if ( context_ == nullptr )
        throw std::runtime_error( "Construction of zmq context failed!" );
}

context::~context()
{
#ifndef BUILD_DLL
    assert( context_ );
    int zmq_ctx_term_value = zmq_ctx_term( context_ ); //cleanup

    assert( zmq_ctx_term_value == 0 ); //throwing an exception is not allowed here!
#endif
}

void* context::get() const // could be protected by befriending socket.
{
    return context_;
}

}
