#include <nett/detail/socket_base.h>

#include <zmq.h>
#include <cassert>

namespace nett
{
socket_base::socket_base( const context &context ) : context_( context )
{
}

socket_base::~socket_base()
{
#ifndef BUILD_DLL
    int zmq_close_return_value = zmq_close( socket_ );
    assert( zmq_close_return_value == 0 );
#endif
}

void* socket_base::get() const //maybe protected for friends?
{
    return socket_;
}

} //namespace nett
 