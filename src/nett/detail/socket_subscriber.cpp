#include <nett/detail/socket_subscriber.h>

#include <zmq.h>
#include <cassert>
#include <cstring>
#include <stdexcept>

namespace nett
{
socket_subscriber::socket_subscriber( const context &context, bool subscribe_to_all  )
    : socket_base( context )
{
    socket_ = zmq_socket( context_.get(), ZMQ_SUB );
    assert( socket_ != nullptr );

    if ( socket_ == nullptr )
        throw std::runtime_error( "Construction of zmq sub socket failed" );

    if ( subscribe_to_all )
        set_subscription();
}

socket_subscriber::socket_subscriber ( const context &context, const std::string endpoint, bool subscribe_to_all )
    : socket_subscriber( context, subscribe_to_all )
{
    if ( !connect( endpoint ) )
        throw std::runtime_error( "Could not connect to specified endpoint" );
}

socket_subscriber::socket_subscriber (const context &context, const std::string endpoint, const byte_stream &subscription_header)
    : socket_subscriber( context, endpoint )
{
    unset_subscription(); //remove default none filter
    set_subscription( subscription_header ); //set the new one
}

bool socket_subscriber::connect( const std::string endpoint ) const
{
    int zmq_connect_return_value = zmq_connect( socket_, endpoint.c_str() );

    if ( zmq_connect_return_value == 0 )
        return true;
    else
        return false;
}

bool socket_subscriber::disconnect( const std::string endpoint ) const
{
    int zmq_disconnect_return_value = zmq_disconnect( socket_, endpoint.c_str() );

    if ( zmq_disconnect_return_value == 0 )
        return true;
    else
        return false;
}

message_t socket_subscriber::receive() const
{
    message_t message_event;
    
    //store for later use
    message_event.routing = receive_routing();
    message_event.content = receive_content();

    return message_event;
}

byte_stream socket_subscriber::receive_routing() const
{
    //first message contains routing information
    zmq_msg_t routing_message;
    int zmq_msg_init_return_value = zmq_msg_init( &routing_message );
    assert( zmq_msg_init_return_value == 0 );

    if ( zmq_msg_init_return_value != 0 )
        throw std::runtime_error( "zmq message init failed" );

    int zmq_msg_recv_return_value = zmq_msg_recv( &routing_message, socket_, 0 );
    assert( zmq_msg_recv_return_value != -1 );

    if ( zmq_msg_recv_return_value == -1 )
        throw std::runtime_error( "zmq message received failed" );

    //retrieve size of message in bytes
    size_t message_len = zmq_msg_size( &routing_message );

    assert( message_len != 0 );

    if ( message_len == 0 )
        throw std::runtime_error( "zmq received empty message" );

    //reserve a vector with the byte count to store message content
    byte_stream routing_data( message_len );

    std::memcpy( &routing_data[ 0 ], zmq_msg_data( &routing_message ), message_len );

    zmq_msg_close( &routing_message );

    return routing_data;
}

byte_stream socket_subscriber::receive_content() const
{
    //check we got the second message with the actual event content
    int more;
    size_t more_size = sizeof( more );
    int zmq_getsockopt_return_value = zmq_getsockopt( socket_, ZMQ_RCVMORE, &more, &more_size );
    assert( zmq_getsockopt_return_value == 0 );

    //second message contains event data
    zmq_msg_t content_message;
    int zmq_msg_init_return_value = zmq_msg_init( &content_message );
    assert( zmq_msg_init_return_value == 0 );

    if ( zmq_msg_init_return_value != 0 )
        throw std::runtime_error( "zmq message init failed" );

    int zmq_msg_recv_return_value = zmq_msg_recv( &content_message, socket_, 0 );
    assert( zmq_msg_recv_return_value != -1 );

    if ( zmq_msg_recv_return_value == -1 )
        throw std::runtime_error( "zmq message received failed" );

    //retrieve size of message in bytes
    size_t message_len = zmq_msg_size( &content_message );

    assert( message_len != 0 );

    if ( message_len == 0 )
        throw std::runtime_error( "zmq received empty message" );

    //reserve a vector with the byte count to store message content
    byte_stream message_data( message_len );

    std::memcpy( &message_data[ 0 ], zmq_msg_data( &content_message ), message_len );

    zmq_msg_close( &content_message );

    return message_data;
}

void socket_subscriber::unset_subscription( const byte_stream &header )
{
    int zmq_setsockopt_return_value = zmq_setsockopt( socket_, ZMQ_UNSUBSCRIBE, &header[0], header.size() );
    assert( zmq_setsockopt_return_value == 0 );

    if ( zmq_setsockopt_return_value != 0 )
        throw std::runtime_error( "zmq unsubscribing from topic failed" );
}

void socket_subscriber::unset_subscription()
{
    int zmq_setsockopt_return_value = zmq_setsockopt( socket_, ZMQ_UNSUBSCRIBE, nullptr, 0 );
    assert( zmq_setsockopt_return_value == 0 );

    if ( zmq_setsockopt_return_value != 0 )
        throw std::runtime_error( "zmq unsubscribing from all topics failed" );
}

void socket_subscriber::set_subscription( const byte_stream &subscription_header )
{
    int zmq_setsockopt_return_value = zmq_setsockopt( socket_, ZMQ_SUBSCRIBE, &subscription_header[ 0 ], subscription_header.size() ); //subscribe to this topic
    assert( zmq_setsockopt_return_value == 0 );

    if ( zmq_setsockopt_return_value != 0 )
        throw std::runtime_error( "set socket option for subscriber failed" );
}

void socket_subscriber::set_subscription()
{
    int zmq_setsockopt_return_value = zmq_setsockopt( socket_, ZMQ_SUBSCRIBE, nullptr, 0 );
    assert( zmq_setsockopt_return_value == 0 );

    if ( zmq_setsockopt_return_value != 0 )
        throw std::runtime_error( "set socket option for subscriber failed" );
}

} //namespace nett
