#include <nett/detail/socket_publisher.h>

#include <zmq.h>
#include <cassert>
#include <cstring>
#include <stdexcept>

namespace nett
{

socket_publisher::socket_publisher( const context &context ) : socket_base( context )
{
    socket_ = zmq_socket( context_.get(), ZMQ_PUB );
    assert( socket_ != nullptr );

    if ( socket_ == nullptr )
        throw std::runtime_error( "Construction of zmq pub socket failed" );
}

socket_publisher::socket_publisher( const context &context, const std::string endpoint )
    : socket_publisher(context)
{
    if ( !bind( endpoint ) )
        throw std::runtime_error( "Binding socket failed" );
}

bool socket_publisher::bind( const std::string endpoint ) const
{
    int zmq_bind_return_value = zmq_bind( socket_, endpoint.c_str() );
    assert( zmq_bind_return_value == 0 ); //not sure if this should be restricted. Could be a valid operation

    if ( zmq_bind_return_value == 0 )
        return true;
    else
        return false;
}

bool socket_publisher::unbind( const std::string endpoint ) const
{
    int zmq_unbind_return_value = zmq_unbind( socket_, endpoint.c_str() );
    assert( zmq_unbind_return_value == 0 ); //not sure if this should be restricted. Could be a valid operation

    if ( zmq_unbind_return_value == 0 )
        return true;
    else
        return false;
}

void socket_publisher::send(const byte_stream &data, bool send_more ) const
{
    zmq_msg_t message;
        
    int zmq_msg_init_size_return_value = zmq_msg_init_size( &message, data.size() );
    assert ( zmq_msg_init_size_return_value == 0 );

    if ( zmq_msg_init_size_return_value != 0 )
        throw std::runtime_error( "zmq message size could not be initialized" );
    
    std::memcpy( zmq_msg_data( &message ), &data[0], data.size() ); //note: Several C++ compilers transform suitable memory-copying loops to std::memcpy calls. 
    
    int zmq_msg_send_return_value = -1;
    
    if ( send_more )
        zmq_msg_send_return_value = zmq_msg_send( &message, socket_, ZMQ_SNDMORE );
    else
        zmq_msg_send_return_value = zmq_msg_send( &message, socket_, 0 );

    assert( zmq_msg_send_return_value != -1 );

    zmq_msg_close( &message );

    if ( zmq_msg_send_return_value == -1 )
        throw std::runtime_error( "zmq sending message failed" );
}

void socket_publisher::send( const byte_stream &data, const byte_stream &header) const
{
    send(header, true);
    send(data);
}

} //namespace nett

