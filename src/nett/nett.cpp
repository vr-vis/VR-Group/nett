#include <nett/nett.h>

namespace nett {
static std::unique_ptr< nett::singleton<slot_factory > > slot_factory_singleton_ptr;
}

void nett::initialize( std::string endpoint )
{
    if ( !slot_factory_singleton_ptr )
    {
        slot_factory_singleton_ptr = std::make_unique<nett::singleton<slot_factory>>( endpoint );
    }
    else
        throw std::runtime_error( "nett already initialized" );
}

nett::singleton<nett::slot_factory>& nett::get()
{
    return *slot_factory_singleton_ptr.get();
}

std::string nett::get_endpoint()
{
    return get().get_endpoint();
}

