set( RelativeDir "src/nett" )
set( RelativeSourceGroup "src/nett" )

set( DirFiles 
  slot_address.cpp
  nett.cpp
  detail/context.cpp
  detail/socket_base.cpp
  detail/socket_publisher.cpp
  detail/socket_subscriber.cpp
  detail/slot_factory.cpp
  detail/slot_base.cpp
  detail/publisher_threadsafe.cpp
#	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

set( SubDirFiles "" )
foreach( Dir ${SubDirs} )
	list( APPEND SubDirFiles "${RelativeDir}/${Dir}/_SourceFiles.cmake" )
endforeach()

foreach( SubDirFile ${SubDirFiles} )
	include( ${SubDirFile} )
endforeach()

