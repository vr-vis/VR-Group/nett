#include <nett/slot_address.h>

#include <stdexcept>

namespace nett {

bool nett::slot_address::operator==( const slot_address& rhs ) const
{
    return ( protocol == rhs.protocol && endpoint == rhs.endpoint &&
        port == rhs.port && tag == rhs.tag );
}

nett::slot_address::slot_address( const std::string &url, const std::string &slot_tag ) : tag( slot_tag )
{
    std::size_t pos_slash = url.rfind( "/" );
    std::size_t pos_colon = url.rfind( ":" );

    if ( pos_slash != std::string::npos )
    {
        //jump one '/' forward
        for ( size_t i = 0; i < pos_slash + 1; ++i )
            protocol += url[ i ];
    }
    else
        throw std::runtime_error( "invalid url specified" );

    if ( pos_slash != std::string::npos )
    {
        for ( size_t i = pos_slash + 1; i < pos_colon; ++i )
            endpoint += url[ i ];
    }
    else
        throw std::runtime_error( "invalid url specified" );

    std::string port_as_string;
    for ( size_t i = pos_colon + 1; i < url.size(); ++i )
    {
        port_as_string += url[ i ];
    }

    port = std::stoi( port_as_string );
}

std::string nett::slot_address::address_to_string() const
{
    std::string return_value;
    return_value += protocol + endpoint + ":" + std::to_string( port );
    return return_value;
}

} //namespace nett
