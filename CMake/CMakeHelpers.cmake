macro (add_event arg1 event_name sub_dir)
  set (dir "${CMAKE_SOURCE_DIR}/" )
  set (outdir "${CMAKE_BINARY_DIR}/nett" )

  if  ( NOT sub_dir STREQUAL "" )
    set (dir "${dir}/${sub_dir}")
    set (outdir "${outdir}/${sub_dir}")
  endif ()

  file(MAKE_DIRECTORY ${outdir})

  add_custom_command(
    COMMAND "${PROTOBUF_PROTOC_EXECUTABLE}" 
    ARGS "${dir}/${event_name}.proto" "--proto_path=${dir}" "--cpp_out=${outdir}"
    DEPENDS "${dir}/${event_name}.proto"
    OUTPUT
    "${outdir}/${event_name}.pb.h" "${outdir}/${event_name}.pb.cc" )
     
  list (APPEND ${arg1}_EVENT_SOURCE_FILES
    "${outdir}/${event_name}.pb.h" "${outdir}/${event_name}.pb.cc")

endmacro()
