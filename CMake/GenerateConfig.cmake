# Define install locations and components
install(TARGETS nett EXPORT nettTargets
  ARCHIVE DESTINATION lib COMPONENT dev
  LIBRARY DESTINATION lib COMPONENT lib
  RUNTIME DESTINATION bin COMPONENT lib
  INCLUDES DESTINATION include
)

# Define public headers installation
foreach( File ${PublicHeaders} )
  get_filename_component( dir ${File} DIRECTORY )
  install(FILES "${PROJECT_SOURCE_DIR}/include/nett/${File}"
    DESTINATION include/nett/${dir}
    COMPONENT dev )
endforeach()

# Create config files and install rules
set( ConfigPackageLocation "share/nett/CMake" )
set( ConfigPackageBuildLocation
  "${CMAKE_CURRENT_BINARY_DIR}/${ConfigPackageLocation}" )

include( CMakePackageConfigHelpers )

write_basic_package_version_file(
  "${CMAKE_CURRENT_BINARY_DIR}/${ConfigPackageLocation}/nettConfigVersion.cmake"
  VERSION ${nett_VERSION}
  COMPATIBILITY AnyNewerVersion )

export( EXPORT nettTargets
  FILE "${CMAKE_CURRENT_BINARY_DIR}/${ConfigPackageLocation}/nettTargets.cmake"
  NAMESPACE Upstream::
)

configure_file( ${PROJECT_SOURCE_DIR}/CMake/nettConfig.cmake.in
  "${ConfigPackageBuildLocation}/nettConfig.cmake"
  COPYONLY
)

install( EXPORT nettTargets
  FILE
    nettTargets.cmake
  DESTINATION
    ${ConfigPackageLocation}
)

install(
  FILES
    "${ConfigPackageBuildLocation}/nettConfig.cmake"
    "${ConfigPackageBuildLocation}/nettConfigVersion.cmake"
  DESTINATION
    ${ConfigPackageLocation}
  COMPONENT
    dev
)
