#include <iostream>

#include <nett/nett.h>
#include <nett/schema/string_message.pb.h>

int main( int, char** )
{
  // Using a unique port for each nett service.
  std::string publish_address( "tcp://127.0.0.1:4001" );

  // Initialize nett and reserve port for publishing.
  nett::initialize( publish_address );

  // Create an input slot to receive "string_message" type data.
  auto slot_in = nett::make_slot_in< string_message >( );

  // Connect the slot to the publishing address for the "my_custom_event" topic.
  slot_in->connect( nett::slot_address( "tcp://127.0.0.1:4000", "my_custom_event" ));

  while( true )
  {
    // (Blocking) Retrieve the received message.
    string_message messageIn = slot_in->receive( );
    std::cout << "Received: " << messageIn.value( ) << std::endl;
  }

  // This will never be reached.
  return 0;

}
