#include <thread>
#include <chrono>
#include <iostream>

#include <nett/nett.h>
#include <nett/schema/string_message.pb.h>

int main( int, char** )
{

  // Using a unique port for each nett service.
  std::string publish_address( "tcp://127.0.0.1:4000" );

  // Initialize net publisher on given end point ip:port address.
  nett::initialize( publish_address );

  // Create a slot for publishing "string_message" type data on "my_custom_event" topic.
  auto slot_out = nett::make_slot_out< string_message >( "my_custom_event" );

  unsigned int counter = 0;
  while( counter < 100 )
  {
    string_message message_out;
    std::string content( "Message out #" );
    content += std::to_string( counter );

    // Set message content.
    message_out.set_value( content );

    // Publish message through output slot.
    slot_out->send( message_out );

    std::cout << "Sent message #" << counter << std::endl;

    // Wait for a second.
    std::this_thread::sleep_for( std::chrono::seconds( 1 ));

    counter++;
  }

  return 0;
}
