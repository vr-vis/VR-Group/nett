#ifndef UTILS_908439083_H
#define UTILS_908439083_H

#include <vector>
#include <string>

namespace nett
{

using byte_stream = std::vector<unsigned char>;

template <class ProtoBufMessage>
nett::byte_stream serialize_unchecked(const ProtoBufMessage &message)
{
	nett::byte_stream return_stream(message.ByteSize());
	message.SerializeToArray(&return_stream[0], message.ByteSize());
	return return_stream;
}


template <class ProtoBufMessage>
nett::byte_stream serialize_checked( const ProtoBufMessage &message )
{
	if ( message.ByteSize() ) //check if message does not contain default types only that are not serialzed
	{
		nett::byte_stream return_stream( message.ByteSize() );
		message.SerializeToArray( &return_stream[0], message.ByteSize() );
		return return_stream;
	}
	else // construct a 'fake' message indicating that we should deserialize a default message
	{
		const std::string name = message.GetTypeName();
		nett::byte_stream return_stream( message.GetTypeName().size() );
		std::copy( name.begin(), name.end(), return_stream.begin() );

		return return_stream;
	}
}

template <class ProtoBufMessage>
ProtoBufMessage deserialize_unchecked( const nett::byte_stream& byte_buffer )
{
    ProtoBufMessage message;
	
	message.ParseFromArray(&byte_buffer[0], static_cast<int>(byte_buffer.size()));
	return message;
}

template <class ProtoBufMessage>
ProtoBufMessage deserialize_checked(const nett::byte_stream& byte_buffer)
{
	ProtoBufMessage message;

	//check if steam is a 'fake' message
	std::string name(reinterpret_cast< char const* >(&byte_buffer[0]), byte_buffer.size() );

	if (name == message.GetTypeName())
		return message;
	else
	{
		message.ParseFromArray(&byte_buffer[0], static_cast<int>(byte_buffer.size()));
		return message;
	}
}

} //namespace nett    

#endif // UTILS_908439083_H
