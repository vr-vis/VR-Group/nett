#ifndef SLOT_IN_985984_H
#define SLOT_IN_985984_H

#include <zmq.h>

#include <nett/slot_address.h>
#include <nett/detail/context.h>
#include <nett/detail/socket_subscriber.h>
#include <nett/detail/slot_factory.h>

#include <mutex>

#include <iostream>

#include <nett/schema/routing_message.pb.h>

namespace nett {

template <class T>
class slot_in final
{
public:
    using event_type = T;

    slot_in( const nett::context &context, const std::string &inproc_endpoint, slot_factory &factory )
        : inproc_subscriber_( context, inproc_endpoint )
        , slot_factory_( factory )
        , inproc_endpoint_( inproc_endpoint )
    {
        inproc_subscriber_.unset_subscription();
    }

    //convenience for Bernd :)
    bool connect( const std::string endpoint, const std::string slot_tag )
    {
        return connect( nett::slot_address( endpoint, slot_tag ) );
    }

    bool connect( const nett::slot_address &slot_address )
    {
        routing_message subscription_header;
        subscription_header.set_slot_tag( slot_address.tag );
        subscription_header.set_socket( slot_address.address_to_string() );

        //inproc_mutex_.lock();
        inproc_subscriber_.set_subscription( nett::serialize_unchecked( subscription_header ) );
        //inproc_mutex_.unlock();

        return slot_factory_.connect_in_slot( slot_address.address_to_string() );
    }

    void disconnect( const nett::slot_address &slot_address )
    {
        inproc_mutex_.lock();
        inproc_subscriber_.unset_subscription();
        inproc_mutex_.unlock();

        slot_factory_.disconnect_in_slot( slot_address.address_to_string() );
    }

    event_type receive()
    {
        inproc_mutex_.lock();
        nett::message_t message = inproc_subscriber_.receive();
        inproc_mutex_.unlock();
        return nett::deserialize_checked< event_type >( message.content );
    }

    event_type receive_non_blocking()
    {
        while( true )
        {
            zmq_pollitem_t item;

            item.socket = inproc_subscriber_.get();
            item.events = ZMQ_POLLIN;

            inproc_mutex_.lock();
            int zmq_poll_count = zmq_poll( &item, 1, poll_time_ );

            if ( zmq_poll_count > 0 )
            {
                nett::message_t message = inproc_subscriber_.receive();
                inproc_mutex_.unlock();
                return nett::deserialize_checked< event_type >( message.content );
            }
            else
            {
                inproc_mutex_.unlock();
                std::this_thread::sleep_for( std::chrono::microseconds( poll_time_ ) );//note: give time to other threads trying to lock mutex
            }
        }
    }

private:
    nett::socket_subscriber inproc_subscriber_;
    slot_factory &slot_factory_;  
    std::mutex inproc_mutex_;
    long poll_time_ = 200;
    std::string inproc_endpoint_;
};

}//namespace nett

#endif //SLOT_IN_985984_H
