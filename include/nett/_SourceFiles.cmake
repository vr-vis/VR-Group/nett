set( RelativeDir "include/nett" )
set( RelativeSourceGroup "include/nett" )

set( PublicHeaders
  utils.h
  slot_out.h
  slot_in.h
  slot_address.h
  nett.h
  detail/context.h
  detail/socket_base.h
  detail/socket_publisher.h
  detail/socket_subscriber.h
  detail/slot_factory.h
  detail/slot_base.h
  detail/publisher_threadsafe.h
)

set( DirFiles
  ${PublicHeaders}
#	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

set( SubDirFiles "" )
foreach( Dir ${SubDirs} )
	list( APPEND SubDirFiles "${RelativeDir}/${Dir}/_SourceFiles.cmake" )
endforeach()

foreach( SubDirFile ${SubDirFiles} )
	include( ${SubDirFile} )
endforeach()

