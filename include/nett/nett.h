#ifndef NETT_8954894_H
#define NETT_8954894_H

#include <nett/slot_in.h>
#include <nett/slot_out.h>
#include <nett/utils.h>
#include <nett/detail/slot_factory.h>

namespace nett {

template <class T>
class singleton
{
public:
    singleton( std::string endpoint )
        : endpoint_( endpoint )
    {
    }

    T& get() const
    {
        static T object( endpoint_ );//note: this is only safe in a multi threaded environment if your compiler supports 'magic statics'
        return object;
    }

    std::string get_endpoint()
    {
        return endpoint_;
    }

private:
    const std::string endpoint_;
};

//#ifdef BUILD_DLL
#define _DLLAPI extern "C" __declspec(dllexport)
//_DLLAPI void initialize( std::string endpoint );
void initialize( std::string endpoint );
//#else
//void initialize( std::string endpoint );
//#endif

std::string get_endpoint();

nett::singleton< slot_factory >& get();

template <class event_type>
std::shared_ptr< slot_out <event_type > > make_slot_out( const std::string slot_tag )
{
    return get().get().make_slot_out< event_type >( slot_tag );
}

template <class event_type>
std::shared_ptr< slot_in <event_type> > make_slot_in()
{
    return get().get().make_slot_in< event_type >();
}

} //namespace nett    

#endif // NETT_8954894_H
