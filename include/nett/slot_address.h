#ifndef SLOT_ADDRESS_879453890_H
#define SLOT_ADDRESS_879453890_H

#include <string>

namespace nett {

struct slot_address 
{
    slot_address(const std::string &url, const std::string &slot_tag);

    std::string address_to_string() const;
    bool operator==( const slot_address& rhs ) const;

    std::string protocol;
    std::string endpoint;
    const std::string tag;
    unsigned int port;
};

}//namespace nett

#endif //SLOT_ADDRESS_879453890_H
