#ifndef SLOT_OUT_89238_H
#define SLOT_OUT_89238_H

#include <nett/detail/publisher_threadsafe.h>
#include <nett/detail/slot_base.h>

namespace nett {

template <class event_type>
class slot_out final : public slot_base
{
public:

    slot_out( const std::string slot_tag, nett::publisher_threadsafe &publisher )
        : slot_base( slot_tag )
        , publisher_threadsafe_( publisher )
    {
    }

    void send( const event_type& event_content )
    {
        publisher_threadsafe_.send( slot_tag_, nett::serialize_checked( event_content ) );
    }

private:
    nett::publisher_threadsafe& publisher_threadsafe_;
};

} //namespace nett

#endif //SLOT_OUT_89238_H
