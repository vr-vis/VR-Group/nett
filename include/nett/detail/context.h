#ifndef CONTEXT_9804321890342_H
#define CONTEXT_9804321890342_H

namespace nett
{

class context final
{
public:
    context();
    ~context();

    void* get() const; //@decide: could be protected by befriending socket.

private:
    void *context_ = nullptr;
};

} //namespace nett

#endif //CONTEXT_9804321890342_H
