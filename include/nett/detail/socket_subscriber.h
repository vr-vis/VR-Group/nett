#ifndef SOCKET_SUBSCRIBER_2342345_H
#define SOCKET_SUBSCRIBER_2342345_H

#include <nett/detail/socket_base.h>
#include <nett/utils.h>

#include <string>


namespace nett
{
struct message_t
{
    byte_stream routing;
    byte_stream content;
};

class socket_subscriber : public socket_base
{
public:
    /**
     * Note: Will only subscribe to everything!
     */
    explicit socket_subscriber( const context &context, bool subscribe_to_all = true );
    
    /**
     * Note: Will only subscribe to everything!
     */
    socket_subscriber( const context &context, const std::string endpoint, bool subscribe_to_all = true );
    
    /**
     * Note: Will only subscribe to topic
     */
    socket_subscriber( const context &context, const std::string endpoint, const byte_stream &subscription_header);

    /**
     * The connection will not be performed immediately but as needed by �MQ. 
     * Thus a successful invocation of zmq_connect() does not indicate that a physical connection was or can actually be established.
     */ 
    bool connect( const std::string endpoint ) const;
    
    /**
     * The zmq_disconnect() function shall disconnect a socket specified by the socket argument from the endpoint specified by the endpoint argument.
     */
    bool disconnect( const std::string endpoint ) const;

    /**
     * Sets a topic filter for the subscriber: all topics not matching this tag shall not be received.
     */
    void set_subscription( const byte_stream &header ); //subscribe to bit mask
    void set_subscription(); //subscribe to everything

    void unset_subscription( const byte_stream &header );
    void unset_subscription(); //remove all topic subscription

    message_t receive() const;

private:

    byte_stream receive_routing() const;
    byte_stream receive_content() const;
    
};

} //namespace nett

#endif //SOCKET_SUBSCRIBER_2342345_H
