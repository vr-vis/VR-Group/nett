#ifndef SLOT_BASE_79084532_H
#define SLOT_BASE_79084532_H

#include <string>

namespace nett
{

class slot_base
{
public:
    const std::string get_slot_tag() const;
    virtual ~slot_base();
protected:
    slot_base( const std::string &slot_tag );

    const std::string slot_tag_;
};

} //namespace nett

#endif //SLOT_BASE_79084532_H
