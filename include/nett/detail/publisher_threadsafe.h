#ifndef PUBLISHER_THREADSAFE_903245_H
#define PUBLISHER_THREADSAFE_903245_H

#include <nett/detail/context.h>
#include <nett/detail/socket_publisher.h>

#include <mutex>

namespace nett {

class publisher_threadsafe
{
public:
    publisher_threadsafe( const nett::context &context, const std::string endpoint );

    void send( const std::string slot_tag, const nett::byte_stream &content );

private:
    const nett::socket_publisher publisher_;
    std::mutex mutex_;
    const std::string endpoint_;
};

} //namespace nett

#endif //PUBLISHER_THREADSAFE_903245_H
