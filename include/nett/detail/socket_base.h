#ifndef SOCKET_BASE_9090234_H
#define SOCKET_BASE_9090234_H

#include <nett/detail/context.h>

namespace nett
{

class socket_base
{
public:
    virtual ~socket_base();
    void* get() const; //maybe protected for friends?
 
protected:
    socket_base( const context &context );

    void *socket_ = nullptr;
    const context &context_;
};

} //namespace nett

#endif //SOCKET_BASE_9090234_H
