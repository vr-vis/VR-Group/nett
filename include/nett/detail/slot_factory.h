#ifndef SLOT_FACTORY_903093_H
#define SLOT_FACTORY_903093_H

#include <nett/detail/context.h>
#include <nett/detail/socket_publisher.h>
#include <nett/detail/socket_subscriber.h>
#include <nett/detail/publisher_threadsafe.h>
#include <nett/slot_out.h>

#include <memory>
#include <thread>
#include <atomic>

namespace nett {

template <class T>
class slot_in;

class slot_factory final
{
public:
    slot_factory( const std::string publisher_endpoint );
    ~slot_factory();

    template <class event_type>
    std::shared_ptr< nett::slot_out <event_type > > make_slot_out( const std::string slot_tag )
    {
        return std::make_shared< nett::slot_out <event_type> >( slot_tag, publisher_threadsafe_ );
    }

    template <class event_type>
    std::shared_ptr< nett::slot_in <event_type> > make_slot_in()
    {
        return std::make_shared< slot_in < event_type > >( context_, inproc_endpoint_, *this );
    }

    //note: should only be called by a slot_in
    bool connect_in_slot( const std::string endpoint );
    void disconnect_in_slot( const std::string endpoint );

private:

    void deliver();
    void kill();

    bool is_connected(const std::string endpoint);
    void remove_connection(const std::string endpoint);

	/**
	 * Note: This is an evil hack: it's a pointer which gets only released when nett is not linked against a dll!
	 * The reason is, that the python interpreter does strange things when this static object is destroyed and the local thread is not joined correctly
	 * If you have an idea to fix this, feel free!
	 */
    std::mutex* connect_mutex_; //locks access to connecting slot_in's;

    const std::string inproc_endpoint_ = "inproc://#1";
    const nett::context context_;
    const nett::socket_subscriber subscriber_;
    const nett::socket_publisher inproc_publisher_;
    nett::publisher_threadsafe publisher_threadsafe_;
    long poll_time_ = 2000;
    
    std::atomic<bool> run_thread_;
    std::thread thread_;//note: this is only allowed to kick in when mutex construction is done!

    /**
     * Note: zmq delivers the same message serveral times if slot_in's are connected to one endpoint.
     * Therefore, we cache all established connections and drop multiple connection attempts.
     */

    std::vector<std::string> endpoint_connections_;

};

} //namespace nett


#endif //SLOT_FACTORY_903093_H
