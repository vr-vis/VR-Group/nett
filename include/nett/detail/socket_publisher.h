#ifndef SOCKET_PUBLISHER_2342345_H
#define SOCKET_PUBLISHER_2342345_H

#include <nett/detail/socket_base.h>
#include <nett/utils.h>
#include <string>

namespace nett
{

class socket_publisher final : public socket_base
{
public:
    socket_publisher( const context &context );
    socket_publisher( const context &context, const std::string endpoint );

    bool bind( const std::string endpoint ) const;
    bool unbind( const std::string endpoint ) const;

    /**
     * - Does not take ownership of the data.
     * - It is a blocking call.
     * - Will copy data vector to zmq message data.
     */
    void send(const byte_stream &data, bool send_more = false) const;

    void send(const byte_stream &data, const byte_stream &header) const;
};

} //namespace nett

#endif //SOCKET_PUBLISHER_2342345_H
