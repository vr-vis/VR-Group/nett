'nett' is the "nimble event transport toolkit" which is intended as a simple network layer for communicating between different applications.

Copyright (c) 2014-2017 RWTH Aachen University, Germany, Virtual Reality & Immersive Visualization Group.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this program. If not, see [http://www.gnu.org/licenses/].

Requirements:
 * Catch for unit testing
 * zmq 4.x without the C++ bindings
 * protobuf 3.x
 * c++14 compiler
 * CMake 3.x

Notes:
 * Catch is integrated in this repository as a single header file.
   For convenience, the CMake environment is already referencing the shipped Catch header. However, you can configure the CATCH_INCLUDE_DIR CMake variable to adjust this behavior to your preferences.

 * For event generation with protobuf, we stick to the schema version 3